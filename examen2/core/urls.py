from django.urls import path

from core import views

app_name = "core"

urlpatterns = [
    ##### USUARIOS URLS #####
    path('list/usuarios/', views.ListUsuario.as_view(), name="list_u"),
    path('new/usuarios/', views.NewUsuario.as_view(), name="create_u"),
    path('update/usuarios/<int:pk>/', views.UpdateUsuario.as_view(), name="update_u"),
    path('delete/usuarios/<int:pk>/', views.DeleteUsuario.as_view(), name="delete_u"),

    ##### ToDo URLS #####
    path('list/todo/', views.ListToDo.as_view(), name="list_t"),
    path('new/todo/', views.NewToDo.as_view(), name="create_t"),
    path('update/todo/<int:pk>/', views.UpdateToDo.as_view(), name="update_t"),
    path('delete/todo/<int:pk>/', views.DeleteToDo.as_view(), name="delete_t"),

    #### EXTRA URLS ####
    path('list/todo/pending/', views.ListToDoPending.as_view(), name="list_tp"),
    path('list/todo/pendingidt/', views.ListToDoPendingIDT.as_view(), name="list_tidt"),
    path('list/todo/pendingidtp/', views.ListToDoPendingIDTP.as_view(), name="list_tidtp"),
    path('list/todo/pendingidtpr/', views.ListToDoPendingIDTPR.as_view(), name="list_tidtpr"),

    path('list/usuarios/pending/', views.ListUsuariosPending.as_view(), name="list_up"),
    path('list/usuarios/pendingr/', views.ListUsuariosPendingR.as_view(), name="list_upr"),
    path('list/usuarios/pendingnr/', views.ListUsuariosPendingNR.as_view(), name="list_upnr"),
]