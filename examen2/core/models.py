from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.


class Usuarios(models.Model):
    username = models.CharField(max_length=50, default="Pedro", unique=True)

    def __str__(self):
        return self.username
    

class ToDo(models.Model):
    title = models.CharField(max_length=100, default="agregar titulo")
    user = models.ForeignKey(Usuarios, on_delete=models.CASCADE)
    pending = models.BooleanField(default=False)
    resolved = models.BooleanField(default=False)

    def __str__(self):
        return self.title
