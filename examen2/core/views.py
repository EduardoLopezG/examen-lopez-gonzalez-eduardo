from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy

# Create your views here.

from core import models
from core import forms


### CRUD USUARIOS


# # # C R E A T E # # #
class NewUsuario(generic.CreateView):
    template_name = "core/create_u.html"
    model = models.Usuarios
    form_class = forms.NewUsuariosForm
    success_url = reverse_lazy("core:list_u")


# # L I S T
class ListUsuario(generic.ListView):
    template_name = "core/list_u.html"
    queryset = models.Usuarios.objects.all()


# # # U P D A T E # # #
class UpdateUsuario(generic.UpdateView):
    template_name = "core/update_u.html"
    model = models.Usuarios
    form_class = forms.UpdateUsuariosForm
    success_url = reverse_lazy("core:list_u")


# # # D E L E T E # # #
class DeleteUsuario(generic.DeleteView):
    template_name = "core/delete_u.html"
    model = models.Usuarios
    success_url = reverse_lazy("core:list_u")


### CRUD Todo


# # # C R E A T E # # #
class NewToDo(generic.CreateView):
    template_name = "core/create_t.html"
    model = models.ToDo
    form_class = forms.NewToDoForm
    success_url = reverse_lazy("core:list_t")

# # L I S T
class ListToDo(generic.ListView):
    template_name = "core/list_t.html"
    queryset = models.ToDo.objects.all()


# # # U P D A T E # # #
class UpdateToDo(generic.UpdateView):
    template_name = "core/update_t.html"
    model = models.ToDo
    form_class = forms.UpdateToDoForm
    success_url = reverse_lazy("core:list_t")
    context_object_name = 'todo'


# # # D E L E T E # # #
class DeleteToDo(generic.DeleteView):
    template_name = "core/delete_t.html"
    model = models.ToDo
    success_url = reverse_lazy("core:list_t")

### EXTRA LISTS

class ListToDoPending(generic.ListView):
    template_name = "core/list_tp.html"
    queryset = models.ToDo.objects.filter(pending=True)


class ListToDoPendingIDT(generic.ListView):
    template_name = "core/list_tidt.html"
    queryset = models.ToDo.objects.filter(pending=True)

class ListToDoPendingIDTP(generic.ListView):
    template_name = "core/list_tidtp.html"
    queryset = models.ToDo.objects.filter(pending=True, resolved=False)

class ListToDoPendingIDTPR(generic.ListView):
    template_name = "core/list_tidtpr.html"
    queryset = models.ToDo.objects.filter(pending=True, resolved=True)

class ListUsuariosPending(generic.ListView):
    template_name = "core/list_up.html"
    queryset = models.ToDo.objects.filter(pending=True)

class ListUsuariosPendingR(generic.ListView):
    template_name = "core/list_upr.html"
    queryset = models.ToDo.objects.filter(pending=True, resolved=True)

class ListUsuariosPendingNR(generic.ListView):
    template_name = "core/list_upnr.html"
    queryset = models.ToDo.objects.filter(pending=True, resolved=False)