from django import forms

from core import models

class NewUsuariosForm(forms.ModelForm):
    class Meta:
        model = models.Usuarios
        fields = [
            "username"
        ]
        widgets = {
            "username": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el  username"})
        }


class UpdateUsuariosForm(forms.ModelForm):
    class Meta:
        model = models.Usuarios
        fields = [
            "username"
        ]
        widgets = {
            "username": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el  username"})
        }


class NewToDoForm(forms.ModelForm):
    class Meta:
        model = models.ToDo
        fields = [
            "title",
            "user",
            "pending",
            "resolved"

        ]
        widgets = {
            "title": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el grantgoal name!"}),
            "user": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "pending": forms.CheckboxInput(attrs={"class":"form-check-input"}),
            "resolved": forms.CheckboxInput(attrs={"class":"form-check-input"})
        }


class UpdateToDoForm(forms.ModelForm):
    class Meta:
        model = models.ToDo
        fields = [
            "title",
            "user",
            "pending",
            "resolved"

        ]
        widgets = {
            "title": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el titulo"}),
            "user": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "pending": forms.CheckboxInput(attrs={"class":"form-check-input"}),
            "resolved": forms.CheckboxInput(attrs={"class":"form-check-input"})
        }