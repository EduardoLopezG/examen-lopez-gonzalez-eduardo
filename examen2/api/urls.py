from django.urls import path

from api import views

app_name = "api"

urlpatterns = [
    ### USUARIOS SERIALIZERS URLS
    path('u/list/usuarios/', views.ListUsuariosAPIView.as_view(), name="list_u_api"),
    path('u/create/usuarios/', views.CreateUsuariosAPIView.as_view(), name="create_u_api"),
    path('u/update/usuarios/<int:pk>/', views.UpdateUsuariosAPIView.as_view(), name="update_u_api"),

    ### ToDo SERIALIZERS URLS
    path('t/list/todo/', views.ListToDoAPIView.as_view(), name="list_u_api"),
    path('t/create/todo/', views.CreateToDoAPIView.as_view(), name="create_u_api"),
    path('t/update/todo/<int:pk>/', views.UpdateToDoAPIView.as_view(), name="update_u_api"),
]
