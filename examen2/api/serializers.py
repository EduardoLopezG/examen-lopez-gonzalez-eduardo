from rest_framework import serializers


from core.models import Usuarios
from core.models import ToDo


class ListUsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuarios
        fields = [
            "username"
        ]

class CreateUsuariosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuarios
        fields = "__all__"


class UpdateUsuariosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuarios
        fields = [
            "username"
        ]

class ListToDoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = [
            "title",
            "user",
            "pending",
            "resolved"
        ]

class CreateToDoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = "__all__"

class UpdateToDoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = [
            "title",
            "user",
            "pending",
            "resolved"
        ]