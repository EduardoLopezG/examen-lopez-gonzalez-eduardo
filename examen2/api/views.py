from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics

from core.models import Usuarios
from core.models import ToDo
from .serializers import ListUsuarioSerializer, CreateUsuariosSerializer, UpdateUsuariosSerializer, ListToDoSerializer, CreateToDoSerializer, UpdateToDoSerializer
# Create your views here.


#### API GRANTGOAL ####

## CREATE
class CreateUsuariosAPIView(generics.CreateAPIView):
    serializer_class = CreateUsuariosSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


# LIST
class ListUsuariosAPIView(APIView):
    def get(self, request):
        queryset = Usuarios.objects.all()
        data = ListUsuarioSerializer(queryset, many=True).data
        return Response(data)


## UPDATE
class UpdateUsuariosAPIView(generics.UpdateAPIView):
    serializer_class = UpdateUsuariosSerializer
    queryset = Usuarios.objects.all()


## CREATE
class CreateToDoAPIView(generics.CreateAPIView):
    serializer_class = CreateToDoSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
    
## LIST
class ListToDoAPIView(APIView):
    def get(self, request):
        queryset = ToDo.objects.all()
        data = ListToDoSerializer(queryset, many=True).data
        return Response(data)
    
## UPDATE
class UpdateToDoAPIView(generics.UpdateAPIView):
    serializer_class = UpdateToDoSerializer
    queryset = Usuarios.objects.all()


